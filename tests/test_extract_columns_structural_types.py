import json
import os
import unittest

from d3m import container, utils
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, extract_columns_structural_types, column_parser


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=utils.JsonEncoder))


class ExtractColumnsByStructuralTypesPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = primitive.produce(inputs=dataset)

        dataframe = call_metadata.value

        hyperparams_class = column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = primitive.produce(inputs=dataframe)

        dataframe = call_metadata.value

        hyperparams_class = extract_columns_structural_types.ExtractColumnsByStructuralTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = extract_columns_structural_types.ExtractColumnsByStructuralTypesPrimitive(hyperparams=hyperparams_class.defaults().replace({'structural_types': ('int',)}))

        call_metadata = primitive.produce(inputs=dataframe)

        dataframe = call_metadata.value

        self.assertIs(dataframe, dataframe.metadata.for_value)

        self._test_metadata(dataframe.metadata)

    def test_can_accept(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        inputs_metadata = dataset.metadata.set_for_value(None)

        hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        dataframe_metadata = dataset_to_dataframe.DatasetToDataFramePrimitive.can_accept(method_name='produce', arguments={'inputs': inputs_metadata}, hyperparams=hyperparams_class.defaults())

        self.assertTrue(dataframe_metadata)

        hyperparams_class = column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        dataframe_metadata = column_parser.ColumnParserPrimitive.can_accept(method_name='produce', arguments={'inputs': dataframe_metadata}, hyperparams=hyperparams_class.defaults())

        self.assertTrue(dataframe_metadata)

        hyperparams_class = extract_columns_structural_types.ExtractColumnsByStructuralTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        outputs_metadata = extract_columns_structural_types.ExtractColumnsByStructuralTypesPrimitive.can_accept(method_name='produce', arguments={'inputs': dataframe_metadata}, hyperparams=hyperparams_class.defaults().replace({'structural_types': ('int',)}))

        self.assertTrue(outputs_metadata)

        self.assertIsNone(outputs_metadata.for_value)

        self._test_metadata(outputs_metadata)

    def _test_metadata(self, metadata):
        self.maxDiff = None

        self.assertEqual(convert_metadata(metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS,))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 2,
            }
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'int',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 1))), {
            'name': 'species',
            'structural_type': 'int',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })


if __name__ == '__main__':
    unittest.main()
